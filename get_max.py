def get_max(list):
    if len(liste) == 0:
        raise ValueError("Keine leere Liste")
    for element in liste:
        if type(element) not in [int, float]:
            raise ValueError("Nur Zahlen")
    
    max = list[0]
    for a in list[1:]:
        if a > max:
            max = a
    return max

#print(get_max([3, -12, 4, 12, -4, 65, 4, 7])) # Test um zu sehen ob die def funktioniert.