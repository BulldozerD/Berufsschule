import unittest
from get_max import get_max

class Testgitlab(unittest.TestCase):
        """Unittest für cal_average(num)"""
        def test_gitlab(self):
            """ Basistest: valide Eingaben """
        list = [3, -12, 4, 12, -4, 65, 4, 7]
        self.assertEqual(65, get_max(list), "Liste mit positiven und negativen Elementen")
        self.assertEqual(5, get_max([5]), "Liste mit einem Element")
        self.assertEqual(101.3, get_max([101.3, 55.5]), "Liste mit Kommazahlen")
        self.assertEqual(-5, get_max([-10, -5, -9]), "Liste mit negativen Zahlen")

         def test_values(self):
        """ Keine Zahl in der Liste """
        self.assertRaises(ValueError, get_max, [])


if __name__ == '__main__':
    unittest.main()
